function loading() {
	app.dialog.progress("Loading...");
    setTimeout(function () {
      app.dialog.close();
    }, 1000);
}	

konfigurasidb();
cek_login();

// Dropdown with ajax data
autocompleteDropdownAjax = app.autocomplete.create({
	inputEl: '#autocomplete-dropdown-ajax',
	openIn: 'dropdown',
	preloader: true, //enable preloader
	/* If we set valueProperty to "id" then input value on select will be set according to this property */
	valueProperty: 'name', //object's "value" property name
	textProperty: 'name', //object's "text" property name
	limit: 10, //limit to 20 results
	dropdownPlaceholderText: 'Masukkan Nama Barang',
	source: function (query, render) {
	  var autocomplete = this;
	  var results = [];
	  if (query.length === 0) {
		render(results);
		return;
	  }
	  // Show Preloader
	  autocomplete.preloaderShow();

	  // Do Ajax request to Autocomplete data
	  app.request({
		url: 'https://monitoring.belajarsaja.com/api_roy/json.php?fungsi=data_tiket',
		method: 'GET',
		dataType: 'json',
		//send "query" to server. Useful in case you generate response dynamically
		data: {
		  query: query,
		},
		success: function (data) {
		  // Find matched items
		  for (var i = 0; i < data.length; i++) {
			if (data[i].name.toLowerCase().indexOf(query.toLowerCase()) >= 0)
				$("#harga").val(data[i].harga);
				$("#id_barang").val(data[i].id);
				$("#nama_barang").val(data[i].name); 
				results.push(data[i]);
		  }
		  // Hide Preoloader
		  autocomplete.preloaderHide();
		  // Render items by passing array with result items
		  render(results);
		}
	  }); 
	},
	/*on: {
	change: function (value) {
	  console.log(value[0].name);
	  $.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "cari_barang", nama_barang: value[0].name}, function(result){
		var dt = "";
		var id = "";
		var nama = "";
		$.each(result, function(i, kolom){
			var id_barang = kolom.id_barang;
			var nama_barang = kolom.nama_barang;
			var harga = kolom.harga;
			var foto = kolom.foto;
			var rp = "Rp. "
			dt += harga;
			nama += nama_barang;
			id += id_barang;

		});
		if (dt == "") {
			app.dialog.alert("Data tidak ditemukan!", "Peringatan");
		}else{
			$("#harga").val(dt);
			$("#id_barang").val(id);
			$("#nama_barang").val(nama);
		}
	})
	},
  },*/
  });


function konfigurasidb(){
	db = openDatabase("posdb", "1.0", "Database POS Percobaan", 4*1024*1024);
	db.transaction(function(tx){
		tx.executeSql("CREATE TABLE IF NOT EXISTS barang (kode_barang VARCHAR(10), nama_barang VARCHAR(100), jumlah VARCHAR(100), harga_barang INT(10), PRIMARY KEY(kode_barang))");
	});

}

function cek_login() {
	if (localStorage.getItem("id_user") === null) {
		app.popup.open("#login");
	}else{
		tampildata();
		penjualan();
		pengunjung();
		tampil_toko();
		laporan_hari_ini();
		tampil_user();
	}
}

function login() {

	var username = $("#username_login").val();
	var password = $("#password_login").val();

	if(username == "" || password == ""){
		app.dialog.alert("Harap isi username & password", "Peringatan");
	}else{
		loading();
		var url = "https://monitoring.belajarsaja.com/api_roy/json.php";
		$.ajax({
			url: url,
			method: "POST",
			data: {fungsi: "cek_login", username: username, password: password},
			cache: "false",
			success: function(y){
				if(y.length === 0){
					app.dialog.alert("Username & password Salah", "Peringatan");
				}else{
					$.each(JSON.parse(y), function(i, kolom){
						var id_user = kolom.id_user;
						var username = kolom.username;
						var password = kolom.password;
						var nama_user = kolom.nama_user;
						
						//app.dialog.alert("Berhasil Login", "Peringatan", function() {
							localStorage.setItem("id_user", username);
							//window.localStorage.clear();
							location.reload(true, function () {
								cek_login();
							});							
						//});
					});
				}
			},
			error: function(error){
				app.dialog.alert("Jaringan Sedang Bermasalah, Periksa Kembali Jaringan Internet Anda","Gagal");
			}
		});
		return false;
	}
}

function keluar() {
	app.dialog.confirm("Apakah yakin ingin keluar?", "Peringatan", function() {
		window.localStorage.clear();
		location.reload(true, function () {
			cek_login();
		});							
	});
}

function tampildata(){
	db.transaction(function(tx){
		tx.executeSql("SELECT rowid, nama_barang, harga_barang, kode_barang, jumlah FROM barang", [], function(tx, results){
			jmldata = results.rows.length;
			if(jmldata > 0){
				var a = "";
				var total = 0;
				for(i=0; i<jmldata; i++){
					var kode = results.rows.item(i).kode_barang;
					var rowid = results.rows.item(i).rowid;
					var nama = results.rows.item(i).nama_barang;
					var jumlah = results.rows.item(i).jumlah;
					var harga = results.rows.item(i).harga_barang;
					total += parseFloat(harga * jumlah);
					a += '\
						<div class="row display-flex padding justify-content-space-between align-items-center">\
			        		<div class="col-80" style="font-size: 12px">'+nama+'</div>\
			        		<div  class="col-10" style="font-size: 12px">'+jumlah+' </div>\
			        		<button class="col-10 button color-red" data-id="'+rowid+'" onclick="hapus_cart(this)" style="padding-top: 6px;"><span class="material-icons"> delete </span></button>\
			        		\
			        		<input type="hidden" class="kode" name="kode[]" value="'+kode+'">\
			        		<input type="hidden" class="jml" name="jml[]" value="'+jumlah+'">\
			        	</div>\
					';

				}
				$("#total").html(total);
				$("#total2").html(total);
				$("#cart").html(a);
				$("#keranjang").show();
			}else{
				$("#keranjang").hide();
			}
		});
	});
}


function cari(){
	loading();
	var nama = $("#nama_barang").val();
	if (nama == "") {
		app.dialog.alert("Data tidak ditemukan!", "Peringatan");
	}else{
		$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "cari_barang", nama_barang: nama}, function(result){
			var dt = "";
			var id = "";
			$.each(result, function(i, kolom){
				var id_barang = kolom.id_barang;
				var nama_barang = kolom.nama_barang;
				var harga = kolom.harga;
				var foto = kolom.foto;
				var rp = "Rp. "
				dt += harga;
				id += id_barang;

			});
			if (dt == "") {
				app.dialog.alert("Data tidak ditemukan!", "Peringatan");
			}else{
				$("#harga").val(dt);
				$("#id_barang").val(id);
			}
		})
	}
}
barang();
function barang() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "data_barang"}, function(result){
		var hasil = "";
		$.each(result, function(i, kolom){
			var id_barang = kolom.id_barang;
			var nama_barang = kolom.nama_barang;
			var harga = kolom.harga;
			var rp = "Rp. ";
			hasil += '<option value="' + nama_barang + '">' + nama_barang + '</option>';
		});
		$("#list").html(hasil);	
	});
}
function cari_laporan(){
	loading();
	var tgl_awal = $("#tgl_awal").val();
	var tgl_akhir = $("#tgl_akhir").val();
	

	if (tgl_awal == "" || tgl_akhir == "") {
		app.dialog.alert("Data tidak ditemukan!", "Peringatan");
		return;
	}
		$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "cari_laporan", awal: tgl_awal, akhir: tgl_akhir}, function(result){
			var dt = "";
			var total = 0;
			$.each(result, function(i, kolom){
				var id_transaksi = kolom.id_transaksi;
				var tgl_beli = kolom.tgl;
				var nama_barang = kolom.nama_barang;
				var jumlah = kolom.jumlah;
				var harga = kolom.harga * kolom.jumlah;
				total +=parseFloat(harga);
				dt += '<tr>\
			          <td class="label-cell">'+id_transaksi+'</td>\
			          <td class="numeric-cell">'+tgl_beli+'</td>\
			          <td class="numeric-cell">'+nama_barang+'</td>\
			          <td class="numeric-cell">'+jumlah+'</td>\
			          <td class="numeric-cell">'+harga+'</td>\
				</tr>';

			});
			$("#tbl_laporan").html(dt);
			$("#jumlah_total").html('<b>Rp. '+total+'</b>');
			
		})
}

function laporan_hari_ini() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "penjualan"}, function(result){
		var dt = "";
		var total = 0;
		$.each(result, function(i, kolom){
			var id_transaksi = kolom.id_transaksi;
			var id_detail = kolom.id_detail;
			var nama_barang = kolom.nama_barang;
			var tgl_beli = kolom.tgl;
			var jumlah = kolom.jumlah;
			var harga = kolom.harga * kolom.jumlah;
			total +=parseFloat(harga);
			dt += '<tr>\
		          <td class="label-cell">'+id_transaksi+'</td>\
		          <td class="numeric-cell">'+tgl_beli+'</td>\
		          <td class="numeric-cell">'+nama_barang+'</td>\
		          <td class="numeric-cell">'+jumlah+'</td>\
		          <td class="numeric-cell">'+harga+'</td>\
		          <td class="actions-cell">\
		            <a class="link icon-only" data-id="'+id_transaksi+'" data-tgl="'+tgl_beli+'" onclick="print_ulang(this)">\
		              <i class="icon material-icons md-only">print</i>\
		            </a>\
		          </td>\
			</tr>';
		});
		var pop = '\
				<div class="sheet-modal my-sheet-swipe-to-step2" style="border-top-left-radius: 15px; border-top-right-radius: 15px; height:auto; --f7-sheet-bg-color: #fff;">\
					<div class="sheet-modal-inner">\
					<div class="sheet-modal-swipe-step">\
						<div class="display-flex padding justify-content-space-between align-items-center">\
						</div>\
						<div class="padding-horizontal padding-bottom">\
							<a class="button button-large button-fill bg">PRINT ULANG</a>\
						</div>\
					</div>\
					</div>\
				</div>';
		$("#tbl_laporan_hari_ini").html(dt);
		$("#print_lagi").html(pop);
		$("#jumlah_total_hari_ini").html('<b>Rp. '+total+'</b>');
	})
}

function print_ulang(el){
	var id_transaksi = $(el).data('id');
	var tgl_beli = $(el).data('tgl');
	
	app.dialog.alert("Apakah yakin ingin Print Ulang?", "Peringatan", function () {
		cetak(id_transaksi, tgl_beli);
	});
}

function total() {
	//var total = "";
	var nama_barang = $("#nama_bgaran").val();
	var harga = $("#harga").val();
	var jumlah = $("#jumlah").val();

	if (jumlah == "") {
		app.dialog.alert("Form jumlah harus diisi!", "Peringatan");
	}else{
		var harga = $("#harga").val();
		var jumlah = $("#jumlah").val();
		var total = harga * jumlah;
		$("#total").html(total);
		$("#total2").html(total);
		$("#item").html(nama_barang);
		$("#qty").html(jumlah);

		$("#nama_barang").val("");
		$("#harga").val("");
		$("#jumlah").val("");
	}
}

function batal() {
	$("#nama_barang").val("");
	$("#harga").val("");
	$("#jumlah").val("");
}

function simpan() {
	var id = $("#id_barang").val();
	var nama_barang = $("#nama_barang").val();
	var harga = $("#harga").val();
	var jumlah = $("#jumlah").val();
	if(nama_barang == "" || harga == "" || jumlah == ""){
		app.dialog.alert("Isian Ada yang Masih Kosong!","Peringatan");
		return;
	}
	db.transaction(function(tx){
		tx.executeSql("INSERT INTO barang VALUES(?,?,?,?)", [id, nama_barang, jumlah, harga],
		function(tx){
			app.dialog.alert("Data Berhasil di Tambahkan","Berhasil");
			tampildata();
			$("#nama_barang").val("");
			$("#harga").val("");
			$("#jumlah").val("");
			//app.views.main.router.back();
		},
		function(){
			app.dialog.alert("Data Gagal di Tambahkan","Gagal");
		});
	});
}

function simpan_laporann() {
    var pus = [];
    var i = 0;
    $(".jml").each(function(index, element) {
        if ($(element).val()!=="") {
        pus.push(
            $(element).val()
        );
        i++;
        }
    });
    
    var puss = [];
    var a = 0;
    $(".kode").each(function(index, element) {
        if ($(element).val()!=="") {
        puss.push(
            $(element).val()
        );
        a++;
        }
    });
	
	
	var url = "https://monitoring.belajarsaja.com/api_roy/json.php";
	$.ajax({
		url: url,
		method: "POST",
		data: {fungsi: "tambah_laporan", nama_barang: puss, jumlah: pus},
		cache: "false",
		success: function(response){
			var myObj = $.parseJSON(response);

			//cetak(puss, pus, myObj.id_transaksi, myObj.tgl);
			cetak(myObj.id_transaksi, myObj.tgl);
			app.dialog.alert("Transaksi berhasil", "Sukses", function () {
				loading();
				location.reload(true);
				
			});
		},
		error: function(){
			app.dialog.alert("Jaringan Sedang Bermasalah, Periksa Kembali Jaringan Internet Anda","Gagal");
		}
	});	
}

function simpan_laporan() {	
	var id = $("#id_barang").val();
	var jumlah = $("#jumlah").val();
	var pembeli = $("#pembeli").val();
	var petugas = localStorage.getItem("id_user");	
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "transaksi", id_tiket: id, jumlah: jumlah, pembeli:pembeli, petugas:petugas}, function(result){
		//console.log(result);
		app.dialog.alert("Transaksi berhasil", "Sukses", function () {
			cetak(result);
				//loading();
				//location.reload(true);
				
			});
	})
}

function hapus_cart(el) {
	var kode = $(el).data('id');
	app.dialog.confirm("Anda Yakin Ingin Menghapus Data Ini ?","Hapus Data",
	function(){
		db.transaction(function(tx){
			tx.executeSql("DELETE FROM barang WHERE rowid=?", [kode],
				function(tx){
					app.dialog.alert("Data Berhasil di Hapus","Berhasil");
					tampildata();
					//app.views.main.router.back();
				},
				function(){
					app.dialog.alert("Data Gagal di Hapus","Gagal");
				}
			);
		});
	})
}

function reset_laporan() {
	$("#tgl_awal").val("");
	$("#tgl_akhir").val("");
	$("#tbl_laporan").html("");
	$("#jumlah_total").html("");
}

function penjualan() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "penjualan"}, function(result){
			var dt = 0;
			$.each(result, function(i, kolom){
				var jumlah = kolom.jumlah;
				var harga = kolom.harga;
				var total = jumlah * harga;
				dt += total;

			});
			
			$("#penjualan_hari_ini").html(dt);
			
		})
}

function pengunjung() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "pengunjung"}, function(result){
		var dt = "";
		$.each(result, function(i, kolom){
			var jumlah = kolom.pengunjung;
			dt += jumlah;

		});
		
		$("#pengunjung_hari_ini").html(dt);
			
	})
}

function tampil_toko() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "kelola_toko"}, function(result){
		$.each(result, function(i, kolom){
			var id_toko = kolom.id_toko;
			var nama_toko = kolom.nama_toko;
			var alamat = kolom.alamat;
			var no_tlp = kolom.no_tlp;
			$("#toko").val(nama_toko);				
			$("#alamat_toko").val(alamat);				
			$("#no_tlp").val(no_tlp);

			$("#header").html(nama_toko);	
			$("#header2").html(nama_toko);	
			$("#header3").html(nama_toko);	
			$("#alamat").html(alamat);	
			$("#no_tlp2").html(no_tlp);	

		});	
						
	})
}

function simpan_toko() {
	var nama_toko = $("#toko").val();
	var alamat = $("#alamat_toko").val();
	var no_tlp = $("#no_tlp").val();

	if (nama_toko == "" || alamat == "" || no_tlp == "") {
		app.dialog.alert("Form harus diisi semua.", "Peringatan");
	}else{
		var url = "https://monitoring.belajarsaja.com/api_roy/json.php";
		$.ajax({
			url: url,
			method: "POST",
			data: {fungsi: "update_toko", nama_toko: nama_toko, alamat: alamat, no_tlp: no_tlp},
			cache: "false",
			success: function(y){
				app.dialog.alert("Berhasil Update Toko", "Sukses", function () {
					loading();
					location.reload(true);
				});
				
			},
			error: function(){
				app.dialog.alert("Jaringan Sedang Bermasalah, Periksa Kembali Jaringan Internet Anda","Gagal");
			}
		});
	}

	
}

function tampil_user() {
	$.getJSON('https://monitoring.belajarsaja.com/api_roy/json.php', {fungsi: "user"}, function(result){
		$.each(result, function(i, kolom){
			var id_user = kolom.id_user;
			var username = kolom.username;
			var password = kolom.password;
			var nama_user = kolom.nama_user;
			var level = kolom.level;
			$("#username").val(username);				
			$("#password").val(password);				
			$("#nama_user").val(nama_user);
			$("#level").val(level);

			var session = localStorage.getItem("id_user","");
			var printer = localStorage.getItem("printer", "");
			var x = printer.split("|");
			$("#nama_user2").html(session);	
			$("#tampil_printer").html(x[0]);	

		});	
						
	})
}

function simpan_user() {
	var username = $("#username").val();
	var password = $("#password").val();
	var nama_user = $("#nama_user").val();
	var level = $("#level").val();

	if (username == "" || password == "" || nama_user == "" || level == "") {
		app.dialog.alert("Form harus diisi semua.", "Peringatan");
	}else{
		var url = "https://monitoring.belajarsaja.com/api_roy/json.php";
		$.ajax({
			url: url,
			method: "POST",
			data: {fungsi: "update_user", username: username, password: password, nama_user: nama_user, level: level},
			cache: "false",
			success: function(y){
				app.dialog.alert("Berhasil Update data", "Sukses", function () {
					loading();
					location.reload(true);
				});
				
			},
			error: function(){
				app.dialog.alert("Jaringan Sedang Bermasalah, Periksa Kembali Jaringan Internet Anda","Gagal");
			}
		});
	}
}



