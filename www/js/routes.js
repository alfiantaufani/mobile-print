var routes = [
  {path: "/", url: "index.html"},
  {path: "/tambah/", componentUrl: "pages/tambah.html"},
  {path: "/transaksi/", componentUrl: "index.html"},
  {path: "/atur_print/", url: "pages/atur_print.html"},
  {path: "/info_kios/", url: "pages/informasi_kios.html"},
  {path: "(.*)", url: "pages/404.html"}
];