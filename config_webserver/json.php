<?php
header('Access-Control-Allow-Origin: *');
    include "koneksi.php";
    $fungsi = $_REQUEST["fungsi"];
    if($fungsi == "data_barang"){
        $data = array();
        $sql = "SELECT * FROM tbl_barang_roy ORDER BY id_barang DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
    }elseif ($fungsi == "cari_barang") {
		$nama = $_REQUEST["nama_barang"];
		$data = array();
		$sql = "SELECT * FROM tbl_barang_roy WHERE nama_barang LIKE '%$nama%' ";
		$q = mysqli_query($koneksi, $sql);
		while ($row = mysqli_fetch_object($q)){
			$data[] = $row;
		}
	    echo json_encode($data);
	}elseif ($fungsi == "laporan"){
	    $data = array();
        $sql = "SELECT * FROM tbl_transaksi_roy ORDER BY id_transaksi DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
	}elseif($fungsi == "cari_laporan"){
	    $tgl_awal = $_REQUEST["awal"];
	    $tgl_akhir = $_REQUEST["akhir"];
	    $data = array();
		$sql = "SELECT * FROM tbl_transaksi_roy JOIN tbl_detail_transaksi_roy ON tbl_transaksi_roy.id_transaksi=tbl_detail_transaksi_roy.id_transaksi JOIN tbl_barang_roy ON tbl_barang_roy.id_barang=tbl_detail_transaksi_roy.id_barang WHERE tbl_transaksi_roy.tgl between '$tgl_awal' AND '$tgl_akhir' ";
		$q = mysqli_query($koneksi, $sql);
		while ($row = mysqli_fetch_object($q)){
			$data[] = $row;
		}
	    echo json_encode($data);
	}elseif($fungsi == "penjualan"){
	    $tgl = date('Y-m-d');
	    $data = array();
		$sql = "SELECT * FROM tbl_barang_roy JOIN tbl_detail_transaksi_roy ON tbl_barang_roy.id_barang=tbl_detail_transaksi_roy.id_barang JOIN tbl_transaksi_roy ON tbl_detail_transaksi_roy.id_transaksi=tbl_transaksi_roy.id_transaksi WHERE tbl_transaksi_roy.tgl = '$tgl' ";
		$q = mysqli_query($koneksi, $sql);
		while ($row = mysqli_fetch_object($q)){
			$data[] = $row;
		}
	    echo json_encode($data);
	}elseif($fungsi == "pengunjung"){
	    $tgl = date('Y-m-d');
	    $data = array();
	    $sql = "SELECT COUNT(id_transaksi) as pengunjung FROM tbl_transaksi_roy WHERE tgl='$tgl'";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
	}elseif($fungsi == "kelola_toko"){
	    $data = array();
        $sql = "SELECT * FROM tbl_toko_roy ORDER BY id_toko DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
	}elseif($fungsi == "update_toko"){
	    $nama_toko = $_REQUEST["nama_toko"];
	    $alamat = $_REQUEST["alamat"];
	    $no_tlp = $_REQUEST["no_tlp"];
	    mysqli_query($koneksi, "UPDATE tbl_toko_roy SET nama_toko='$nama_toko', alamat='$alamat', no_tlp='$no_tlp' WHERE id_toko=1");
	    
	}elseif($fungsi == "user"){
	    $data = array();
        $sql = "SELECT * FROM tbl_user_roy ORDER BY id_user DESC";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
	}elseif($fungsi == "update_user"){
	    $username = $_REQUEST["username"];
	    $password = $_REQUEST["password"];
	    $nama_user = $_REQUEST["nama_user"];
	    $level = $_REQUEST["level"];
	    mysqli_query($koneksi, "UPDATE tbl_user_roy SET username='$username', password='$password', nama_user='$nama_user', level='$level' WHERE id_user=1");
	    
	}elseif($fungsi == "cek_login"){
	    $data = array();
	    $error = array();
	    $username = $_REQUEST["username"];
	    $password = $_REQUEST["password"];
	    $sql = "SELECT * FROM tbl_user_roy WHERE username='$username' AND password='$password'";
        $q = mysqli_query($koneksi, $sql);
        
        if(mysqli_num_rows($q) != 0){
            //echo 1;
            while ($row = mysqli_fetch_object($q)){
                $data[] = $row;
            }
            echo json_encode($data);
        }
	}elseif($fungsi == "cek_print"){
		$nama_barang = $_GET['nama_barang'];
		$jumlah = $_GET['jumlah'];
		$tgl = date('Y-m-d');
		$data = array();
		
		
		for($i=0; $i < count($nama_barang); $i++){
			$jml = $jumlah[$i];
	        $barang = $nama_barang[$i];
			$sql = "SELECT * FROM tbl_barang_roy JOIN tbl_detail_transaksi_roy ON tbl_barang_roy.id_barang=tbl_detail_transaksi_roy.id_barang JOIN tbl_transaksi_roy ON tbl_detail_transaksi_roy.id_transaksi=tbl_transaksi_roy.id_transaksi WHERE tbl_detail_transaksi_roy.id_barang='$nama_barang' AND tbl_detail_transaksi_roy.jumlah='$jumlah' AND tbl_transaksi_roy.tgl='$tgl'";
		}
		
		$q = mysqli_query($koneksi, $sql);
		while ($row = mysqli_fetch_object($q)){
			$data[] = $row;
		}
		echo json_encode($data);
	}elseif($fungsi == "tambah_laporan"){
	    $tgl = date('Y-m-d');
	    $nama_barang = $_REQUEST["nama_barang"];
	    $jumlah = $_REQUEST["jumlah"];
	    
	    mysqli_query($koneksi, "INSERT INTO tbl_transaksi_roy (tgl) VALUES ('$tgl')");
	    $id_transaksi = $koneksi->insert_id;
	    for($i=0; $i < count($nama_barang); $i++){
	        $jml = $jumlah[$i];
	        $barang = $nama_barang[$i];
	        mysqli_query($koneksi, "INSERT INTO tbl_detail_transaksi_roy (id_transaksi, id_barang, jumlah) VALUES ('$id_transaksi', '$barang', '$jml')");
	    }
		$data = array();
        //$sql = "SELECT * FROM tbl_transaksi_roy WHERE id_transaksi='$id_transaksi'";
        $sql = "SELECT * FROM tbl_barang_roy JOIN tbl_detail_transaksi_roy ON tbl_barang_roy.id_barang=tbl_detail_transaksi_roy.id_barang JOIN tbl_transaksi_roy ON tbl_detail_transaksi_roy.id_transaksi=tbl_transaksi_roy.id_transaksi WHERE tbl_transaksi_roy.id_transaksi='$id_transaksi'";
        $q = mysqli_query($koneksi, $sql);
        while ($row = mysqli_fetch_object($q)){
            $data[] = $row;
        }
        echo json_encode($data);
	}
	
?>
