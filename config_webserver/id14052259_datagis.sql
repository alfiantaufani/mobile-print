-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 30 Mar 2021 pada 08.27
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id14052259_datagis`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_barang_roy`
--

CREATE TABLE `tbl_barang_roy` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `foto` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_barang_roy`
--

INSERT INTO `tbl_barang_roy` (`id_barang`, `nama_barang`, `harga`, `foto`) VALUES
(1, 'Mancing', 25000, 'mancing.jpg'),
(2, 'Renang', 30000, 'mancing2.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_detail_transaksi_roy`
--

CREATE TABLE `tbl_detail_transaksi_roy` (
  `id_detail` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_detail_transaksi_roy`
--

INSERT INTO `tbl_detail_transaksi_roy` (`id_detail`, `id_transaksi`, `id_barang`, `jumlah`) VALUES
(4, 20, 1, 2),
(5, 20, 1, 2),
(6, 21, 1, 2),
(7, 21, 1, 2),
(8, 22, 1, 1),
(9, 22, 2, 3),
(10, 23, 2, 3),
(11, 24, 2, 3),
(12, 24, 1, 1),
(13, 25, 2, 2),
(14, 26, 1, 1),
(15, 27, 1, 1),
(16, 28, 1, 1),
(17, 29, 1, 1),
(18, 30, 1, 1),
(19, 31, 1, 1),
(20, 32, 1, 1),
(21, 33, 2, 1),
(22, 34, 2, 1),
(23, 35, 1, 1),
(24, 36, 1, 1),
(25, 37, 1, 1),
(26, 38, 2, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_toko_roy`
--

CREATE TABLE `tbl_toko_roy` (
  `id_toko` int(11) NOT NULL,
  `nama_toko` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `no_tlp` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_toko_roy`
--

INSERT INTO `tbl_toko_roy` (`id_toko`, `nama_toko`, `alamat`, `no_tlp`) VALUES
(1, 'Kolam Pancing & Renang', 'Jl. Adi Sucipto No. 2 Jogoroto Jombang', '081383167850');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi_roy`
--

CREATE TABLE `tbl_transaksi_roy` (
  `id_transaksi` int(11) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_transaksi_roy`
--

INSERT INTO `tbl_transaksi_roy` (`id_transaksi`, `tgl`) VALUES
(8, '2021-03-09'),
(9, '2021-03-09'),
(10, '2021-03-09'),
(11, '2021-03-10'),
(12, '2021-03-10'),
(13, '2021-03-10'),
(14, '2021-03-10'),
(15, '2021-03-10'),
(16, '2021-03-10'),
(17, '2021-03-10'),
(18, '2021-03-10'),
(19, '2021-03-10'),
(20, '2021-03-10'),
(21, '2021-03-10'),
(22, '2021-03-10'),
(23, '2021-03-12'),
(24, '2021-03-12'),
(25, '2021-03-12'),
(26, '2021-03-13'),
(27, '2021-03-16'),
(28, '2021-03-16'),
(29, '2021-03-16'),
(30, '2021-03-16'),
(31, '2021-03-16'),
(32, '2021-03-24'),
(33, '2021-03-24'),
(34, '2021-03-24'),
(35, '2021-03-24'),
(36, '2021-03-24'),
(37, '2021-03-25'),
(38, '2021-03-25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_roy`
--

CREATE TABLE `tbl_user_roy` (
  `id_user` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama_user` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `tbl_user_roy`
--

INSERT INTO `tbl_user_roy` (`id_user`, `username`, `password`, `nama_user`, `level`) VALUES
(1, 'admin', 'admin', 'Administrator', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_barang_roy`
--
ALTER TABLE `tbl_barang_roy`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `tbl_detail_transaksi_roy`
--
ALTER TABLE `tbl_detail_transaksi_roy`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indeks untuk tabel `tbl_toko_roy`
--
ALTER TABLE `tbl_toko_roy`
  ADD PRIMARY KEY (`id_toko`);

--
-- Indeks untuk tabel `tbl_transaksi_roy`
--
ALTER TABLE `tbl_transaksi_roy`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `tbl_user_roy`
--
ALTER TABLE `tbl_user_roy`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_barang_roy`
--
ALTER TABLE `tbl_barang_roy`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_detail_transaksi_roy`
--
ALTER TABLE `tbl_detail_transaksi_roy`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `tbl_toko_roy`
--
ALTER TABLE `tbl_toko_roy`
  MODIFY `id_toko` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksi_roy`
--
ALTER TABLE `tbl_transaksi_roy`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `tbl_user_roy`
--
ALTER TABLE `tbl_user_roy`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
